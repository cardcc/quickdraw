
function isInteger(value) {
    return typeof value === 'number' && Number.isInteger(value);
}

function isInstanceOfJogador(obj) {
    return typeof obj === 'object' && obj instanceof Jogador;
}

function isString(value) {
    return typeof value === 'string' || value instanceof String;
}

export function isGetParamDefined(param) {
    const searchParams = new URLSearchParams(location.search);
    return searchParams.has(param);
}


export function getQueryParam(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    const regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    const results = regex.exec(location.search);
    const value = results === null ? '' : results[1];

    try {
        return decodeURIComponent(value);
    } catch (error) {
        //console.error('Error decoding parameter:', error);
        return false;
    }
}

function isValidNome(value) {
    if ( value ) {
        if ( isString(value ) ) {
            if ( value.length > 1 ) {
                return true;
            }
        }
    }
    return false;
}

function isValidForca(value) {
    if ( value ) {

        if ( isString(value)) {
            if(!(/\d/.test(value)))  {
                return false;
            }
            value = parseInt(value);
        }
        if ( isInteger(value) ) {
            if ( value >= 1 && value <= 10) {
                return true;
            }
        }
    }
    return false;
}

export function isValidJogadorValues(values) {

     if ( values ) {
         if ( values.nome) {
             if ( values.forca ) {

                 if ( isValidNome(values.nome) && isValidForca(values.forca) ) {
                     return new Jogador(
                        values.nome,
                        parseInt(values.forca)
                     );
                 }
             }
         }
     }
     return false;
}

export class Jogador {
    constructor(nome, forca) {
        this.nome = nome;
        this.forca = forca;
    }
}

export class Sorteio {

    constructor() {
        this.jogadoresDisponiveis = [];
        this.jogadoresSorteio = [];
    }

    addJogadorToDisponiveis(jogador) {

        if ( isInstanceOfJogador(jogador) ) {

            this.jogadoresDisponiveis.push(jogador);
        }
    }

    clearJogadoresDisponiveis() {

        this.jogadoresDisponiveis = [];
    }

    removeJogador(n) {
        if (n >= 0 && n < this.jogadoresDisponiveis.length) {
            this.jogadoresDisponiveis.splice(n, 1); // The second argument (1) is the number of elements to remove
        } else {
            console.log("Invalid index, element not removed.");
        }
    }

    iniciarSorteio(numeroEquipas, numeroJogadoresPorEquipa) {

        this.jogadoresSorteio = [];
        if ( !numeroEquipas ) {

            return false;
        }

        if ( !numeroJogadoresPorEquipa ) {

            return false;
        }

        if ( !( isInteger(numeroEquipas) && isInteger(numeroJogadoresPorEquipa) ) ) {

            return false;
        }

        if (numeroEquipas <= 0 || numeroJogadoresPorEquipa <= 0) {

            return false;
        }

        if ( (numeroEquipas * numeroJogadoresPorEquipa) > this.jogadoresDisponiveis.length) {

            return false;
        }

        this.jogadoresSorteio = this.generateBalancedTeams(this.jogadoresDisponiveis, numeroEquipas, numeroJogadoresPorEquipa);
        return true;
    }


    generateBalancedTeams(playersAvailable, teamCount, teamSize) {

        const originalLength = playersAvailable.length;
        const playersNecessarios = teamCount*teamSize;
        let players = shuffleArray(playersAvailable);

        if ( playersNecessarios < originalLength ) {
            //se entrei aqui tenho mais jogadores disponíveis
            //do que aqueles que preciso para formar as equipas
            players = shuffleArray(playersAvailable).slice(0, teamCount*teamSize);
        }

        players.sort((a, b) => b.forca - a.forca);

        let teams = Array.from({ length: teamCount }, () => []);

        //selecionar um elemento aleatoriamente independentemente da força
        for (let x = 0; x < teamCount ; x++) {

            // Generate a random index
            const randomIndex = Math.floor(Math.random() * players.length);
            // Remove and get the element at the random index
            const randomElement = players.splice(randomIndex, 1)[0];

            teams[x].push(randomElement);
        }


        while ( players.length > 0) {

            const teamKeysOrderedByForce = orderKeysByLowestForcaSum(teams);
            const half = Math.ceil(teamKeysOrderedByForce.length / 2);
            const firstHalf = teamKeysOrderedByForce.splice(0, half)
            const secondHalf = teamKeysOrderedByForce.splice(-half)

            firstHalf.forEach((item)=>{

                teams[item].push(players.shift());

            });

            secondHalf.reverse().forEach((item)=>{

                teams[item].push(players.pop());

            });

        }

        return shuffleArray(teams);

        function orderKeysByLowestForcaSum(arr) {

            const keySumPairs = arr.map((subarray, key) => ({
                key,
                sum: subarray.reduce((total, element) => total + element.forca, 0),
            }));

            keySumPairs.sort((a, b) => a.sum - b.sum);

            return keySumPairs.map(pair => pair.key);


        }

        function shuffleArray(array) {
            const shuffledArray = [...array]; // Create a copy of the original array
            for (let i = shuffledArray.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]]; // Swap elements at i and j
            }
            return shuffledArray;
        }

    }


    generateSorteioSummary() {
        const result = [];

        for (let i = 0; i < this.jogadoresSorteio.length; i++) {
            const team = this.jogadoresSorteio[i];
            const total = team.reduce((sum, member) => sum + member.forca, 0);
            const jogadores = team.map(({ nome }) => ({ nome }));

            const teamObject = {
                equipa: `team${i + 1}`,
                jogadores,
                'forca-total': total,
            };

            result.push(teamObject);
        }

        return result;
    }



}
