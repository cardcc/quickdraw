export class ContentService {

    constructor(host, appRef) {
        this.host = host;
        this.appRef = appRef;
    }

    getSorteio(contentRef) {

        const url = this.host+'/nuclear-back/app/'+this.appRef+'/content/get/'+contentRef;
        console.log('dudue',url);
        return axios.get(url);
            /*
            .then(function (response) {
                // handle success
                console.log('response',response);
            })
            .catch(function (error) {
                // handle error
                console.log('error',error);
            }).finally(function () {
                console.log('all done')
                // always executed
            });
           */
    }

    newSorteio(content) {

        const url = this.host+'/nuclear-back/app/'+this.appRef+'/content/new';
        const categoryRef = '8f5bd55d-e964-4ea6-80fe-bb0fef5be649';
        const data = new FormData;
        data.append('categoryRef',categoryRef);
        data.append('content',content);
        data.append('contentUpdateDate','23-10-2023 20:32');

        const info =  {
            'categoryRef': categoryRef,
            'content': content,
            'contentUpdateDate': '23-10-2023 20:32',
        };

        return axios.post(url, info, {
                headers: {
                    'Content-Type': 'multipart/form-data', // Important: Set the Content-Type to 'multipart/form-data' for sending form data
                },
            })
            /*
            .then(function (response) {
                console.log('done1',response.data.info[0]);
            })
            .catch(function (error) {
                console.log('error1',error);
            });
            */
    }

}