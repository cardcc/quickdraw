
import {
    //objects
    Jogador,
    Sorteio,

    //functions
    isValidJogadorValues,
    getQueryParam,
    isGetParamDefined


} from "./helper.js";

import {

    ContentService

} from "./service.js";


const sorteio = new Sorteio();
const contentService = new ContentService(
    'https://nuclearback1.blixdigital.com',
    'f0a943e0-8c07-482d-832f-6fdc63e7c16a'
);


$(document).ready(function () {

    /*
    sorteio.jogadoresDisponiveis.push(new Jogador("Carlos",2));
    sorteio.jogadoresDisponiveis.push(new Jogador("Paulo",10));
    sorteio.jogadoresDisponiveis.push(new Jogador("Cesar",3));
    sorteio.jogadoresDisponiveis.push(new Jogador("Chico",2));
    sorteio.jogadoresDisponiveis.push(new Jogador("Simao",1));
    sorteio.jogadoresDisponiveis.push(new Jogador("Alice",2));
    sorteio.jogadoresDisponiveis.push(new Jogador("Miguel",7));
    sorteio.jogadoresDisponiveis.push(new Jogador("Bruno",8));
    sorteio.jogadoresDisponiveis.push(new Jogador("Santos",9));
    sorteio.jogadoresDisponiveis.push(new Jogador("Kula",5));
    sorteio.jogadoresDisponiveis.push(new Jogador("Sara",6));
    sorteio.jogadoresDisponiveis.push(new Jogador("Joao",4));
    */


    refreshTableJogadoresDisponiveis();

    if ( isGetParamDefined('draw') ) {

        // Get the "draw" parameter from the URL
        const drawParam = getQueryParam('draw');
        // Convert the parameter to a JSON object
        if ( drawParam ) {

            contentService.getSorteio(drawParam)
                .then(function (response) {
                    // handle success
                    if ( response.data.status == "success" ) {

                        if ( response.data.info[0].contentRef == drawParam ) {

                            try {

                                displayJogadoresSorteadosListPrintPage();
                                generateJogadoresSorteadslist(response.data.info[0].content);

                            } catch (error) {

                                window.location.href = '/';
                            }
                        }
                    }
                })
                .catch(function (error) {
                    // handle error
                    window.location.href = '/';
                });

        } else {
            window.location.href = '/';
        }
    } else {
        displaySorteioPage();
    }

    function removeJogadorDisponivel(index) {

        sorteio.removeJogador(index);
    }

    function refreshTableJogadoresDisponiveis() {

        // Function to create and populate the table
        const $table = $("#tableJogadoresDisponiveis");
        const $tbody = $table.find("tbody");

        // Clear any existing table data
        $tbody.empty();

        // Loop through the data array and create a row for each object
        $.each(sorteio.jogadoresDisponiveis, function(index, item) {
            const $row = $("<tr>");
            $row.append($("<td>").text(index+1));
            $row.append($("<td>").text(item.nome));
            $row.append($("<td>").text(item.forca));

            // Add the HTML content to a table cell for the action column
            const $actionCell = $("<td>").html("<i id='jogadorDisponivel-"+index+"' class='onHoverChangePointer fa-solid fa-circle-xmark'></i>");
            $row.append($actionCell);

            $tbody.append($row);
        });

        $("[id^='jogadorDisponivel-']").click(function() {

            removeJogadorDisponivel(parseInt(this.id.replace(/^jogadorDisponivel-/, '')));
            refreshTableJogadoresDisponiveis();
        });
    }


    function clearSorteioResult() {
        $("#jogadores-sorteados-list").empty();
        $('#jogadores-sorteados-list-container').css('display', 'none');
    }

    function displaySorteioPage() {
        $('#sorteio-content').css('display', 'block');
    }

    function displayJogadoresSorteadosListPrintPage() {
        $('#jogadores-sorteados-list-print-content').css('display', 'block');
    }

    function displayErroSorteio() {
        $('#sorteio-erro').css('display', 'block');
    }

    function displayErroSorteioList() {
        $('#list-sorteio-erro').css('display', 'block');
    }

    function hideErroSorteio() {
        $('#sorteio-erro').css('display', 'none');
        $('#list-sorteio-erro').css('display', 'none');
    }

    function generateJogadoresSorteadslist(sorteioConfig) {

        const container = $('#jogadores-sorteados-list-print');
        container.empty(); // Clear the existing content

        sorteioConfig.forEach((item, index) => {
            const article = $('<article>');
            const headingsDiv = $('<div class="headings">');
            const h2 = $('<h2>').text(item.equipa);
            const h3 = $('<h3>');
            const i = $('<i class="fa-solid fa-hand-fist fa-xl">').html(`&nbsp; ${item['forca-total']}`);
            h3.append(i);

            headingsDiv.append(h2);
            headingsDiv.append(h3);

            const ul = $('<ul>');

            item.jogadores.forEach((jogador) => {
                const li = $('<li>').text(jogador.nome);
                ul.append(li);
            });

            article.append(headingsDiv);
            article.append(ul);

            container.append(article);
        });
    }

    function refreshJogadoresSorteio() {

        let htmlGenerated = '';
        function generateTeamHTML(team, teamNumber) {
            const totalForca = team.reduce((acc, player) => acc + player.forca, 0);
            const teamHTML = `
                <details open>
                  <summary>Team ${teamNumber} <small class="forca"> | Força ${totalForca}</small></summary>
                  <ul>
                    ${team.map(player => `
                      <li>${player.nome} <small class="forca"> | Força ${player.forca}</small></li>`).join("\n")}
                  </ul>
                </details>`;

            return teamHTML;
        }

        sorteio.jogadoresSorteio.forEach((team, index) => {

            const teamNumber = index + 1;
            htmlGenerated += generateTeamHTML(team, teamNumber);
        });


        $("#jogadores-sorteados-list").html(htmlGenerated);

        return htmlGenerated;
    }


    $("#buttonInserirJogador").click(function (event) {

        event.preventDefault();

        let formInfo = {};
        $.each($('#formJogador').serializeArray(), function(_, kv) {
            formInfo[kv.name] = kv.value;
        });

        let jogador = isValidJogadorValues(formInfo);

        if ( jogador ) {
            sorteio.addJogadorToDisponiveis(jogador);
        }

        refreshTableJogadoresDisponiveis();
        $("#formJogador")[0].reset();

        document.getElementById('formJogadorNome').focus();

    });

    $("#gerarSorteio").click(function (event) {

        hideErroSorteio();

        event.preventDefault();

        clearSorteioResult();

        let result = sorteio.iniciarSorteio( parseInt($('#sorteio-numero-equipas').val()), parseInt($('#sorteio-jogadores-por-equipa').val()));
        if ( !result ) {
            displayErroSorteio();
            return;
        }

        $('#jogadores-sorteados-list-container').css('display', 'block');

        refreshJogadoresSorteio();

    });


    $("#gerarJogadoresPrintList").click(function (event) {

        event.preventDefault();

        // Convert data to a JSON string
        const jsonData = JSON.stringify(sorteio.generateSorteioSummary());

        const link = $(this);

        contentService.newSorteio(jsonData)
            .then(function (response) {

                if ( response.data.status == "success" ) {
                    if ( response.data.info[0].contentRef ) {

                        const href = link.attr('href') + '?draw=' + response.data.info[0].contentRef;
                        window.open(href,"_self");
                    }
                }
            })
            .catch(function (error) {
                displayErroSorteioList();
            });

    });

    $("#jogadoresSorteadosListPrintShareButton").click(function (event) {

        event.preventDefault();

        if (navigator.share) {
            navigator.share( {
                title: 'QuickDraw - Sorteio de equipas para jogo',
                url: window.location.href
            }).then(()=> {
                console.log('Shared');
            });
        }

    });




});